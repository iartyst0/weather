# weather

Решение тестового задания http://telegra.ph/Testovoe-zadanie-android-08-01

### Используемые решения:
+ Язык: *Kotlin*
+ Архитектура MVP: *Moxy*
+ Работа с потоками: *RxJava 2*
+ Сеть: *Retrofit, OkHTTP, GSON*
+ DI: *Dagger 2*
+ Навигация: *Cicerone*
+ Юнит-тесты: *JUnit 4, Mockito, Robolectric*
+ UI-тесты: *Espresso*

Поддерживаются build.flavors для ускоренной сборки >=21 SDK

### Варианты запуска тестов
* Для запуска UI тестов в консоли `./gradlew spoonBaseDebug` <br />
*Должно быть подключено хотя бы одно устройство или эмулятор!*

* Для запуска Unit тестов в консоли `./gradlew app:testBaseDebugUnitTest`