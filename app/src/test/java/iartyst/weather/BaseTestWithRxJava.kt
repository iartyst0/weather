package iartyst.weather

import iartyst.weather.app.WeatherApp
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * Base class for testing. Provides @Before setUp() and @After tearDown()
 * methods initializing for tests and restoring RxJava Schedulers.
 */
@Config(constants = BuildConfig::class,
        sdk = [21],
        manifest = Config.NONE,
        application = WeatherApp::class)
open class BaseTestWithRxJava {
    @Before
    @Throws(Exception::class)
    open fun setUp() {
        TestUtils.initRx()
    }

    @After
    @Throws(Exception::class)
    open fun tearDown() {
        TestUtils.resetRx()
    }
}