package iartyst.weather

import iartyst.weather.app.Api
import iartyst.weather.common.Defaults
import iartyst.weather.mvp.models.repositories.WeatherRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations.initMocks
import org.robolectric.RobolectricTestRunner

/**
 * Author iartyst
 */

@RunWith(RobolectricTestRunner::class)
class DataTest : BaseTestWithRxJava() {
    @Mock
    lateinit var api: Api

    @InjectMocks
    lateinit var repository: WeatherRepository

    @Before
    @Throws(Exception::class)
    override fun setUp() {
        super.setUp()
        initMocks(this)
    }

    @Test
    @Throws(Exception::class)
    fun dataGroup() {
        try {
            repository.getGroup(Defaults.cityIds).subscribe()
        } catch (ignored: NullPointerException) {
        }

        verify<Api>(api).getGroup(TestUtils.any(), TestUtils.any())

    }

    @Test
    @Throws(Exception::class)
    fun dataForecast() {
        try {
            repository.getForecast(Defaults.cityIds[0]).subscribe()
        } catch (ignored: NullPointerException) {
        }

        verify<Api>(api).getForecast(any(Long::class.java), TestUtils.any())
    }

}

