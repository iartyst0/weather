@file:Suppress("unused")

package iartyst.weather

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.mockito.ArgumentMatcher
import org.mockito.Mockito

@Suppress("UNCHECKED_CAST")
/**
 * Utils for tests
 *
 * Created by e.rikov on 21.10.2017.
 */

object TestUtils {

    // For test any() not null
    fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T
    /**
     * Call this method in test before method marked @Before annotation
     * if you need RxJava in your test.
     *
     * initRx sets all schedulers handlers to Schedulers.trampoline()
     */
    @JvmStatic
    fun initRx() {
        // Override RxJava schedulers:
        RxJavaPlugins.reset()
        RxJavaPlugins.setErrorHandler { }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }

        // Override RxAndroid schedulers:
        RxAndroidPlugins.reset()
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    /**
     * Reset RxJava to initial state.
     *
     * Call this method in test after method marked @After annotation if in
     * test before method marked @Before annotation [initRx]
     * was called
     */
    @JvmStatic
    fun resetRx() {
        RxJavaPlugins.reset()
        RxJavaPlugins.setErrorHandler { }
        RxAndroidPlugins.reset()
    }

    fun once() = Mockito.times(1)
}