package iartyst.weather.mvp.presenters

import iartyst.weather.BaseTestWithRxJava
import iartyst.weather.TestUtils
import iartyst.weather.TestUtils.once
import iartyst.weather.app.Api
import iartyst.weather.di.AppComponent
import iartyst.weather.mvp.models.gson.WeatherGroup
import iartyst.weather.mvp.models.repositories.WeatherRepository
import iartyst.weather.mvp.views.`HomeView$$State`
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import ru.terrakok.cicerone.Router

/**
 * Author iartyst
 */
@RunWith(RobolectricTestRunner::class)
class HomePresenterTest : BaseTestWithRxJava() {
    @Mock
    lateinit var api: Api

    @Mock
    lateinit var appComponent: AppComponent

    @Mock
    lateinit var router: Router

    @Spy
    @InjectMocks
    lateinit var repository: WeatherRepository

    @InjectMocks
    lateinit var presenter: HomePresenter

    private val viewState = Mockito.mock(`HomeView$$State`::class.java)

    @Before
    @Throws(Exception::class)
    override fun setUp() {
        super.setUp()

        MockitoAnnotations.initMocks(this)
        presenter.setViewState(viewState)
    }

    @Test
    @Throws(Exception::class)
    fun testFirstAttach() {
        try {
            Mockito.`when`(api.getGroup(TestUtils.any(), TestUtils.any()))
                    .thenReturn(Observable.just(WeatherGroup()))
        } catch (ignored: NullPointerException) {
        }

        presenter.onFirstViewAttach()

        Mockito.verify(viewState, once()).hideRefreshButton()
        Mockito.verify(viewState, once()).setCities(TestUtils.any())
    }

    @Test
    @Throws(Exception::class)
    fun throwNullpointer() {
        try {
            Mockito.`when`(api.getGroup(TestUtils.any(), TestUtils.any()))
                    .thenReturn(Observable.just(mock(WeatherGroup::class.java)))

        } catch (ignored: NullPointerException) {
        }

        presenter.onFirstViewAttach()

        Mockito.verify(viewState, once()).showRefreshButton()
        Mockito.verify(viewState, once()).hideSpinner()
    }
}
