package iartyst.weather.ui.activities;


import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import iartyst.weather.R;
import iartyst.weather.common.Defaults;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static iartyst.weather.ui.activities.AndroidTestUtils.childAtPosition;
import static iartyst.weather.ui.activities.AndroidTestUtils.getText;
import static iartyst.weather.ui.activities.AndroidTestUtils.getViewById;
import static iartyst.weather.ui.activities.AndroidTestUtils.waitSeconds;
import static iartyst.weather.ui.activities.AndroidTestUtils.withRecyclerView;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    private int spinnerPosition = 0;
    private int itemPosition = 0;
    private String weatherText;
    private String dateText;

    @Test
    public void mainActivityTest() {
        for (int i = 0; i < Defaults.INSTANCE.getCityIds().length; i++) {
            spinnerPosition = i;
            itemPosition = getRandom(0, 5);
            weatherText = "";
            dateText = "";

            waitSeconds(5);

            checkSpinner();

            waitSeconds(5);

            checkItemInRecycler();

            goToItemPage();

            checkDataAtItemPage();

            pressBack();
        }
    }

    private void checkSpinner() {
        ViewInteraction appCompatSpinner = onView(
                allOf(withId(R.id.spinner),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.appbar),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatSpinner.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(android.R.id.text1), childAtPosition(
                        childAtPosition(
                                IsInstanceOf.instanceOf(android.widget.FrameLayout.class),
                                0),
                        spinnerPosition),
                        isDisplayed()));

        String text = getText(textView);

        textView.check(matches(withText(text)));

        // Select item in spinner
        DataInteraction appCompatTextView = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(is("android.widget.PopupWindow$PopupBackgroundView")),
                        0))
                .atPosition(spinnerPosition);
        appCompatTextView.perform(click());

        waitSeconds(5);

        // Check what we selected is applied
        ViewInteraction textView2 = onView(
                allOf(withId(android.R.id.text1),
                        childAtPosition(
                                allOf(withId(R.id.spinner),
                                        childAtPosition(
                                                withId(R.id.toolbar),
                                                0)),
                                0),
                        isDisplayed()));
        textView2.check(matches(withText(text)));
    }

    public void checkItemInRecycler() {
        ViewInteraction weatherTextView =  onView(withRecyclerView(R.id.recyclerView)
                .atPositionOnView(itemPosition, R.id.weatherTextView));
        weatherText = getText(weatherTextView);
        weatherTextView.check(matches(withText(weatherText)));

        ViewInteraction dateTextView =  onView(withRecyclerView(R.id.recyclerView)
                .atPositionOnView(itemPosition, R.id.dateTextView));
        dateText = getText(dateTextView);
        dateTextView.check(matches(withText(dateText)));
    }

    public void checkDataAtItemPage() {
        ViewInteraction weatherTextView = getViewById(R.id.weatherTextView);
        weatherTextView.check(matches(withText(weatherText)));

        ViewInteraction dateTextView = getViewById(R.id.dateTextView);
        dateTextView.check(matches(withText(dateText)));
    }

    private void goToItemPage() {
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.recyclerView),
                        childAtPosition(
                                withId(R.id.refreshLayout),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(itemPosition, click()));
    }

    public int getRandom(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }
}
