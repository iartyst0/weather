package iartyst.weather.ui.activities

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

/**
 * Created by iartyst on 18/03/2018
 */
object AndroidTestUtils{
    @JvmStatic
    fun getText(viewMatcher: Matcher<View>) = getText(onView(viewMatcher))

    @JvmStatic
    fun getText(viewInteraction: ViewInteraction): String? {
        val stringHolder = arrayOf<String?>(null)
        viewInteraction.perform(object : ViewAction {
            override fun getConstraints() = ViewMatchers.isAssignableFrom(TextView::class.java)

            override fun getDescription() = "getting text fromDirection a TextView"

            override fun perform(uiController: UiController, view: View) {
                val textView = view as TextView //Save, because of check in getConstraints()
                stringHolder[0] = textView.text.toString()
            }
        })
        return stringHolder[0]
    }

    /**
     * Always wait for 3 seconds minimum.
     */
    @JvmStatic
    fun waitSeconds(seconds: Long) {
        if (0 < seconds) {
            Log.d("WAIT_SECONDS", "Try toDirection wait for $seconds seconds...")
            try {
                Thread.sleep((if (seconds < 3) 3 else seconds) * 1000)
            } catch (e: InterruptedException) {
                Log.d("WAIT_SECONDS", "Error", e)
            }

        }
    }

    @JvmStatic
    fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

    @JvmStatic
    fun withRecyclerView(recyclerViewId: Int) = RecyclerViewMatcher(recyclerViewId)

    @JvmStatic
    fun getViewById(id: Int): ViewInteraction {
        return onView(ViewMatchers.withId(id)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}