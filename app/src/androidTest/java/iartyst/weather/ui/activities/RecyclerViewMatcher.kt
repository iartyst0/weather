package iartyst.weather.ui.activities

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

/**
 * Created by e.rikov on 16.11.2017
 */

class RecyclerViewMatcher(private val recyclerViewId: Int) {

    fun atPosition(position: Int): Matcher<View> {
        return atPositionOnView(position, -1)
    }

    fun atPositionOnView(position: Int, targetViewId: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            internal var resources: Resources? = null
            internal var childView: View? = null

            override fun describeTo(description: Description) {
                var idDescription = Integer.toString(recyclerViewId)
                resources?.let {
                    idDescription = try {
                        it.getResourceName(recyclerViewId)
                    } catch (e: Resources.NotFoundException) {
                        String.format("%d (resource name not found)", recyclerViewId)
                    }

                }

                description.appendText("with id: $idDescription")
            }

            public override fun matchesSafely(view: View): Boolean {
                resources = view.resources

                if (childView == null) {
                    val recyclerView = view.rootView.findViewById(recyclerViewId) as RecyclerView
                    if (recyclerView.id == recyclerViewId) {
                        val viewHolder = recyclerView.findViewHolderForAdapterPosition(position)
                        if (viewHolder != null) {
                            childView = viewHolder.itemView
                            Log.d("RecyclerViewMatcher", "position: $position; tag: " + viewHolder.itemView.tag)
                        } else {
                            val itemsCount = recyclerView.adapter?.itemCount ?: 0
                            if (itemsCount <= position) {
                                val maxItemIndex = itemsCount - 1
                                throw IndexOutOfBoundsException("RecyclerView(itemCount: $itemsCount; max item index: $maxItemIndex; requested index: $position")
                            } else {
                                return false
                            }
                        }
                    } else {
                        return false
                    }
                }

                return if (targetViewId == -1) {
                    view === childView
                } else {
                    val targetView = childView!!.findViewById<View>(targetViewId)
                    view === targetView
                }

            }
        }
    }

}
