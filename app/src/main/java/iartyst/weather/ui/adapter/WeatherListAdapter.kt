package iartyst.weather.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpDelegate
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.item_card.view.*
import iartyst.weather.R
import iartyst.weather.app.WeatherApp
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.presenters.ListAdapterPresenter
import iartyst.weather.mvp.views.ListAdapterView
import javax.inject.Inject

/**
 * Author: iartyst
 */

class WeatherListAdapter(private val weatherList: MutableList<Weather>, mvpDelegate: MvpDelegate<*>, cityId: Long) :
        MvpBaseAdapter<WeatherListAdapter.ViewHolder>(mvpDelegate, cityId.toString()), ListAdapterView {

    @Inject
    lateinit var glide: RequestManager

    @InjectPresenter
    lateinit var listAdapterPresenter: ListAdapterPresenter

    init {
        WeatherApp.appComponent.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(weatherList[position])
    }

    override fun getItemCount(): Int {
        return weatherList.size
    }

    fun swap(weatherList: List<Weather>?) {
        if (weatherList != null && weatherList.isNotEmpty()) {
            this.weatherList.clear()
            this.weatherList.addAll(weatherList)
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bind(weather: Weather) = with(view){
            dateTextView.text = weather.readableDate
            weatherTextView.text = weather.readableWeather
            glide.load(weather.weatherIconUrl).into(iconImageView)

            view.setOnClickListener { listAdapterPresenter.onItemClicked(weather) }
        }
    }
}
