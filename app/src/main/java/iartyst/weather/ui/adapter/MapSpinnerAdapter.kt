package iartyst.weather.ui.adapter

import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.ThemedSpinnerAdapter
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import iartyst.weather.mvp.models.Weather

/**
 * Author: iartyst
 */

class MapSpinnerAdapter(items: SparseArray<Weather>, context: Context) : SparseArrayAdapter<Weather>(), ThemedSpinnerAdapter {

    private val dropDownHelper: ThemedSpinnerAdapter.Helper

    init {
        super.setData(items)
        dropDownHelper = ThemedSpinnerAdapter.Helper(context)
    }

    override fun getDropDownViewTheme(): Resources.Theme? {
        return dropDownHelper.dropDownViewTheme
    }

    override fun setDropDownViewTheme(theme: Resources.Theme?) {
        dropDownHelper.dropDownViewTheme = theme
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val view: View

        view = if (convertView == null) {
            val inflater = dropDownHelper.dropDownViewInflater
            inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        } else {
            convertView
        }

        val textView = view.findViewById<View>(android.R.id.text1) as TextView
        textView.text = getItem(position)?.name
        return view

    }
}
