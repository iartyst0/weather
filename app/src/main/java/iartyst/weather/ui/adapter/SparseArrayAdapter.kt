package iartyst.weather.ui.adapter

import android.util.SparseArray
import android.widget.BaseAdapter

/**
 * Author: iartyst
 */

abstract class SparseArrayAdapter<E> : BaseAdapter() {

    private var mData: SparseArray<E>? = null
    fun setData(data: SparseArray<E>) {
        mData = data
    }

    override fun getCount(): Int {
        return mData?.size() ?: 0
    }

    override fun getItem(position: Int): E? {
        return mData?.valueAt(position)
    }

    override fun getItemId(position: Int): Long {
        return mData?.keyAt(position)?.toLong() ?: -1
    }
}
