package iartyst.weather.ui.adapter

import android.support.v7.widget.RecyclerView

import com.arellomobile.mvp.MvpDelegate

/**
 * Author iartyst
 */
abstract class MvpBaseAdapter<I : RecyclerView.ViewHolder>(private val mParentDelegate: MvpDelegate<*>, private val mChildId: String) : RecyclerView.Adapter<I>() {
    private var mMvpDelegate: MvpDelegate<out MvpBaseAdapter<*>>? = null

    private val mvpDelegate: MvpDelegate<*>?
        get() {
            if (mMvpDelegate == null) {
                mMvpDelegate = MvpDelegate(this)
                mMvpDelegate?.setParentDelegate(mParentDelegate, mChildId)

            }
            return mMvpDelegate
        }

    init {
        mvpDelegate?.onCreate()
        mvpDelegate?.onAttach()

    }
}
