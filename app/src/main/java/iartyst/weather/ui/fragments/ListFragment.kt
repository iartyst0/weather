package iartyst.weather.ui.fragments

import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_list.*
import iartyst.weather.R
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.presenters.ListPresenter
import iartyst.weather.mvp.views.ListView
import iartyst.weather.ui.adapter.WeatherListAdapter

/**
 * Author: iartyst
 */

class ListFragment : MvpAppCompatFragment(), ListView, SwipeRefreshLayout.OnRefreshListener {

    @InjectPresenter
    lateinit var listPresenter: ListPresenter

    private var adapter: WeatherListAdapter? = null


    @ProvidePresenter
    internal fun providePresenter(): ListPresenter {
        return ListPresenter(arguments.getLong(ARG_CITY))
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshLayout.isEnabled = true
        refreshLayout.isRefreshing = true
        refreshLayout.setColorSchemeColors(Color.RED)
        refreshLayout.setOnRefreshListener(this)

    }

    override fun onRefresh() {
        listPresenter.onRefresh()
    }

    override fun setWeather(weatherList: List<Weather>, cityId: Long) {
        refreshLayout.isRefreshing = false

        if (adapter == null) {
            adapter = WeatherListAdapter(weatherList.toMutableList(), mvpDelegate, cityId)
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = adapter
        } else {
            adapter?.swap(weatherList)
        }

    }

    override fun setState(state: Parcelable) {
        recyclerView.layoutManager.onRestoreInstanceState(state)
    }


    override fun onPause() {
        super.onPause()
        if (recyclerView.layoutManager != null) {
            listPresenter.setScrollPosition(recyclerView.layoutManager.onSaveInstanceState())
        }
        if (refreshLayout != null) {
            refreshLayout.isRefreshing = false
            refreshLayout.destroyDrawingCache()
            refreshLayout.clearAnimation()
        }
    }

    companion object {

        private const val ARG_CITY = "ARG_CITY"

        fun newInstance(cityId: Long): ListFragment {
            val fragment = ListFragment()
            val args = Bundle()
            args.putLong(ARG_CITY, cityId)
            fragment.arguments = args
            return fragment
        }
    }
}
