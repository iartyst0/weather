package iartyst.weather.ui.activities

import android.os.Bundle

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.activity_detail.*

import javax.inject.Inject

import iartyst.weather.R
import iartyst.weather.app.WeatherApp
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.presenters.DetailPresenter
import iartyst.weather.mvp.views.DetailView

class DetailActivity : MvpAppCompatActivity(), DetailView {

    @Inject
    lateinit var glide: RequestManager

    @InjectPresenter
    lateinit var detailPresenter: DetailPresenter

    @ProvidePresenter
    internal fun providePresenter(): DetailPresenter {
        val weather = intent.getParcelableExtra<Weather>(ARG_WEATHER)
        return DetailPresenter(weather)
    }

    init {
        WeatherApp.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
    }

    override fun setMainInfo(info: String) {
        weatherTextView.text = info
    }

    override fun setIcon(iconUrl: String) {
        glide.load(iconUrl).into(iconImageView)
    }

    override fun setDate(date: String) {
        dateTextView.text = date
    }

    override fun setPressure(pressure: Float?) {
        pressureTextView.text = resources.getString(R.string.pressure, pressure)
    }

    override fun setWindSpeed(speed: Float?) {
        windTextView.text = resources.getString(R.string.wind, speed)
    }

    companion object {
        const val ARG_WEATHER = "weather"
    }
}
