package iartyst.weather.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.SparseArray
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_main.*
import iartyst.weather.R
import iartyst.weather.app.WeatherApp
import iartyst.weather.common.Const
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.presenters.HomePresenter
import iartyst.weather.mvp.views.HomeView
import iartyst.weather.ui.adapter.MapSpinnerAdapter
import iartyst.weather.ui.fragments.ListFragment
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), HomeView {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @InjectPresenter
    lateinit var homePresenter: HomePresenter

    private val navigator = object : SupportAppNavigator(this, R.id.container) {
        override fun createActivityIntent(screenKey: String, data: Any): Intent? {
            when (screenKey) {
                Const.NAV_ITEM -> {
                    val weather = data as Weather
                    val intent = Intent(this@MainActivity, DetailActivity::class.java)
                    intent.putExtra(DetailActivity.ARG_WEATHER, weather)
                    return intent
                }
            }
            return null
        }

        override fun createFragment(screenKey: String, data: Any): Fragment? {
            when (screenKey) {
                Const.NAV_CITY -> return ListFragment.newInstance(data as Long)
            }
            return null
        }
    }

    init {
        WeatherApp.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar.let { setSupportActionBar(it) }
        supportActionBar?.setDisplayShowTitleEnabled(false)

        spinner.onItemSelectedListener = object : OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, id: Long) {
                homePresenter.onCitySelected(id)
            }
        }

        refreshButton.setOnClickListener {
            homePresenter.onRefreshClicked()
        }
    }

    override fun setCities(cities: SparseArray<Weather>) {
        spinner.visibility = View.VISIBLE
        spinner.adapter = MapSpinnerAdapter(cities, this)

    }

    override fun showRefreshButton() {
        refreshButton.visibility = View.VISIBLE
    }

    override fun hideRefreshButton() {
        refreshButton.visibility = View.GONE
    }

    override fun hideSpinner() {
        spinner.visibility = View.GONE
    }


    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

}
