package iartyst.weather.common

/**
 * Author: iartyst
 */

object Const {
    const val NAV_CITY = "NAV_CITY"
    const val NAV_ITEM = "NAV_ITEM"
    const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
    const val API_ID = "c51a4bda4815a036c043abb1bb8c6a5c"
}
