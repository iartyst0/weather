package iartyst.weather.common

/**
 * Created by iartyst
 */
fun <T> join(array: Array<T>, glue: String): String {
    if (array.isEmpty()) {
        return ""
    }

    val result = StringBuilder()

    result.append(array[0])

    for (i in 1 until array.size) {
        result.append(glue).append(array[i])
    }

    return result.toString()

}
