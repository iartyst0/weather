package iartyst.weather.di

import javax.inject.Singleton

import dagger.Component
import iartyst.weather.di.modules.ApiModule
import iartyst.weather.di.modules.AppModule
import iartyst.weather.di.modules.ImageModule
import iartyst.weather.di.modules.NavigationModule
import iartyst.weather.di.modules.RepositoryModule
import iartyst.weather.di.modules.RetrofitModule
import iartyst.weather.mvp.models.repositories.WeatherRepository
import iartyst.weather.mvp.presenters.HomePresenter
import iartyst.weather.mvp.presenters.ListAdapterPresenter
import iartyst.weather.mvp.presenters.ListPresenter
import iartyst.weather.ui.activities.DetailActivity
import iartyst.weather.ui.activities.MainActivity
import iartyst.weather.ui.adapter.WeatherListAdapter

/**
 * Author: iartyst
 */

@Singleton
@Component(modules = [(ApiModule::class), (AppModule::class), (RetrofitModule::class), (NavigationModule::class), (ImageModule::class), (RepositoryModule::class)])
interface AppComponent {

    fun inject(weatherRepository: WeatherRepository)

    fun inject(homePresenter: HomePresenter)

    fun inject(mainActivity: MainActivity)

    fun inject(listPresenter: ListPresenter)

    fun inject(weatherListAdapter: WeatherListAdapter)

    fun inject(listAdapterPresenter: ListAdapterPresenter)

    fun inject(detailActivity: DetailActivity)
}
