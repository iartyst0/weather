package iartyst.weather.di.modules

import android.content.Context

import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Author: iartyst
 */

@Module(includes = [(AppModule::class)])
class ImageModule {


    @Provides
    @Singleton
    internal fun provideGlide(context: Context): RequestManager {
        return Glide.with(context)
    }

}
