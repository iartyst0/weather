package iartyst.weather.di.modules

import android.content.Context

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Author: iartyst
 */

@Module
class AppModule(private val mContext: Context) {

    @Provides
    @Singleton
    internal fun provideContext(): Context {
        return mContext
    }

}
