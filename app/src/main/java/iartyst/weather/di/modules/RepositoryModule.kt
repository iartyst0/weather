package iartyst.weather.di.modules

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import iartyst.weather.mvp.models.repositories.WeatherRepository

/**
 * Author: iartyst
 */

@Module
class RepositoryModule {

    @Singleton
    @Provides
    internal fun provideWeatherRepository(): WeatherRepository {
        return WeatherRepository()
    }

}
