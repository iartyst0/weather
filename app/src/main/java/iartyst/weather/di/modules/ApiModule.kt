package iartyst.weather.di.modules

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import iartyst.weather.app.Api
import retrofit2.Retrofit

/**
 * Author: iartyst
 */


@Module(includes = [(RetrofitModule::class)])
class ApiModule {

    @Provides
    @Singleton
    internal fun provideApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }
}
