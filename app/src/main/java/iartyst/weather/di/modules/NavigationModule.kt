package iartyst.weather.di.modules

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
 * Author: iartyst
 */


@Module
class NavigationModule {

    lateinit var mCicerone: Cicerone<Router>

    internal val router: Router
        @Provides
        @Singleton
        get() = mCicerone.router

    init {
        initCicerone()
    }

    private fun initCicerone() {
        mCicerone = Cicerone.create(Router())
    }

    @Provides
    @Singleton
    internal fun provideHolder(): NavigatorHolder {
        return mCicerone.navigatorHolder
    }

}

