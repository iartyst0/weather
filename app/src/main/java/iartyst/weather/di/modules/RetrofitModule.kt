package iartyst.weather.di.modules

import android.content.Context

import com.google.gson.FieldNamingPolicy
import com.google.gson.FieldNamingStrategy
import com.google.gson.Gson
import com.google.gson.GsonBuilder

import java.io.File
import java.lang.reflect.Field

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import iartyst.weather.BuildConfig
import iartyst.weather.common.Const
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Author: iartyst
 */

@Module(includes = [(AppModule::class)])
class RetrofitModule {

    private// read from cache for 5 minutes
    val networkInterceptor: (Interceptor.Chain) -> Response
        get() = { chain ->
            val originalResponse = chain.proceed(chain.request())
            val maxAge = 5 * 60
            originalResponse.newBuilder()
                    .header("Cache-Control", "public, max-age=" + maxAge)
                    .build()
        }

    @Provides
    @Singleton
    internal fun provideRetrofit(builder: Retrofit.Builder): Retrofit {
        return builder.baseUrl(Const.BASE_URL).build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofitBuilder(converterFactory: Converter.Factory, context: Context): Retrofit.Builder {

        val httpClient = OkHttpClient.Builder()


        if (BuildConfig.DEBUG) {
            val loggintInterceptor = HttpLoggingInterceptor()
            loggintInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggintInterceptor)
        }


        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("appid", Const.API_ID)
                    .build()

            val requestBuilder = original.newBuilder()
                    .url(url)

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        httpClient.addNetworkInterceptor(networkInterceptor)


        val httpCacheDirectory = File(context.cacheDir, "responses")

        val cache = Cache(httpCacheDirectory, (10 * 1024 * 1024).toLong())

        httpClient.cache(cache)

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(converterFactory)
                .client(httpClient.build())
    }

    @Provides
    @Singleton
    internal fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setFieldNamingStrategy(CustomFieldNamingPolicy())
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .serializeNulls()
                .create()
    }


    private class CustomFieldNamingPolicy : FieldNamingStrategy {
        override fun translateName(field: Field): String {
            var name = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(field)
            name = name.substring(2, name.length).toLowerCase()
            return name
        }
    }

}

