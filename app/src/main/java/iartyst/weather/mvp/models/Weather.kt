package iartyst.weather.mvp.models

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Author: iartyst
 */

@SuppressLint("ParcelCreator")
@Suppress("unused")
@Parcelize
data class Weather (
        @SerializedName("id") var cityId: Int?,
        @SerializedName("weather") private var precipitations: List<Precipitation>?,
        @SerializedName("main") private var details: Details?,
        var visibility: Int?,
        var wind: Wind?,
        private var clouds: Clouds?,
        private var dt: Int?,
        var name: String?
) : Parcelable {
    val pressure: Float?
        get() = details?.pressure

    private val date: Long
        get() = dt?.times(1000L) ?: 0

    val readableDate: String
        get() {
            val simpleDateFormat = SimpleDateFormat("dd.MM HH:mm", Locale.getDefault())
            return simpleDateFormat.format(Date(date))
        }

    val readableWeather: String
        get() = String.format(Locale.getDefault(),
                "%+.0f, %s",
                details?.temp,
                precipitations?.get(0)?.description ?: "")


    val weatherIconUrl: String
        get() = String.format(Locale.getDefault(),
                "http://openweathermap.org/img/w/%s.png",
                precipitations?.get(0)?.icon ?: "")

    fun getWind(): Float? {
        return wind?.speed
    }

    fun getClouds(): Float? {
        return clouds?.all
    }

    @Parcelize
    data class Precipitation(
            var id: Int?,
            var main: String,
            var description: String?,
            var icon: String?
    ) : Parcelable

    @Parcelize
    data class Details(
            var temp: Float?,
            var pressure: Float?,
            var humidity: Float?,
            var tempMin: Float?,
            var tempMax: Float?
    ) : Parcelable

    @Parcelize
    data class Wind(
            var speed: Float?
    ) : Parcelable

    @Parcelize
    data class Clouds(
            var all: Float?
    ) : Parcelable
}
