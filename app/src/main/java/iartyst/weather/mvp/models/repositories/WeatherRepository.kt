package iartyst.weather.mvp.models.repositories

import android.text.TextUtils
import android.util.SparseArray
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import iartyst.weather.app.Api
import iartyst.weather.app.WeatherApp
import iartyst.weather.mvp.models.Weather
import javax.inject.Inject

/**
 * Author: iartyst
 */

open class WeatherRepository {

    @Inject
    lateinit var api: Api

    private var cityIds: Array<Long> = arrayOf()

    private val units: String = "metric"

    constructor() {
        @Suppress("LeakingThis")
        WeatherApp.appComponent.inject(this)
    }

    constructor(api: Api) {
        this.api = api
    }

    fun getGroup(cityIds: Array<Long>): Observable<SparseArray<Weather>> {

        this.cityIds = cityIds

        val ids = TextUtils.join(",", cityIds)

        return api.getGroup(ids, units)
                .subscribeOn(Schedulers.io())
                .map {
                    val weatherArray = SparseArray<Weather>()

                    for (weather in it.list) {
                        weather.cityId?.let { weatherArray.put(it, weather) }
                    }

                    weatherArray
                }
                .observeOn(AndroidSchedulers.mainThread())


    }

    fun getForecast(id: Long): Observable<List<Weather>> {
        return api.getForecast(id, units)
                .subscribeOn(Schedulers.io())
                .map({ it.list })
                .onErrorResumeNext(
                        api.getGroup(TextUtils.join(", ", cityIds), units)
                                .map{ it.list }
                                .flatMap{ Observable.fromIterable(it) }
                                .filter { weather -> weather.cityId?.toLong() == id }
                                .toList()
                                .toObservable()
                )
                .observeOn(AndroidSchedulers.mainThread())

    }

}
