package iartyst.weather.mvp.models.gson

import com.google.gson.annotations.SerializedName

import iartyst.weather.mvp.models.Weather

/**
 * Author: iartyst
 */

open class WeatherGroup(
    @SerializedName("cnt") val count: Int? = null,
    val list: List<Weather> = listOf()
)
