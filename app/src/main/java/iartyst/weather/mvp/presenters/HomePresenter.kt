package iartyst.weather.mvp.presenters

import android.util.SparseArray

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

import javax.inject.Inject

import io.reactivex.annotations.NonNull
import io.reactivex.observers.DefaultObserver
import iartyst.weather.app.WeatherApp
import iartyst.weather.common.Const
import iartyst.weather.common.Defaults
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.models.repositories.WeatherRepository
import iartyst.weather.mvp.views.HomeView
import ru.terrakok.cicerone.Router

/**
 * Author: iartyst
 */

@InjectViewState
open class HomePresenter : MvpPresenter<HomeView>() {

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var repository: WeatherRepository

    private var currentCity: Long = -1

    init {
        @Suppress("LeakingThis")
        WeatherApp.appComponent.inject(this)
    }

    public override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadCities()

    }

    fun onCitySelected(id: Long) {
        if (currentCity != id) {
            router.newRootScreen(Const.NAV_CITY, id)
            currentCity = id
        }
    }

    fun onRefreshClicked() {
        loadCities()
    }

    private fun loadCities() {
        repository.getGroup(Defaults.cityIds)
                .subscribe(object : DefaultObserver<SparseArray<Weather>>() {
                    override fun onNext(@NonNull weatherLongSparseArray: SparseArray<Weather>) {
                        viewState?.hideRefreshButton()
                        viewState?.setCities(weatherLongSparseArray)
                    }

                    override fun onError(@NonNull e: Throwable) {
                        viewState?.showRefreshButton()
                        viewState?.hideSpinner()
                    }

                    override fun onComplete() {

                    }
                })
    }
}
