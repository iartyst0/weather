package iartyst.weather.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.views.DetailView

/**
 * Author iartyst
 */

@InjectViewState
class DetailPresenter(private val weather: Weather) : MvpPresenter<DetailView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.setDate(weather.readableDate)
        viewState.setIcon(weather.weatherIconUrl)
        viewState.setMainInfo(weather.readableWeather)
        viewState.setPressure(weather.pressure)
        viewState.setWindSpeed(weather.wind?.speed)
    }
}
