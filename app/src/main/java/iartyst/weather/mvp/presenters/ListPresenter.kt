package iartyst.weather.mvp.presenters

import android.os.Parcelable

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

import javax.inject.Inject

import io.reactivex.annotations.NonNull
import io.reactivex.observers.DefaultObserver
import iartyst.weather.app.WeatherApp
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.models.repositories.WeatherRepository
import iartyst.weather.mvp.views.ListView
import ru.terrakok.cicerone.Router

/**
 * Author: iartyst
 */

@InjectViewState
class ListPresenter(private val cityId: Long) : MvpPresenter<ListView>() {

    @Inject
    lateinit var weatherRepository: WeatherRepository

    @Inject
    lateinit var router: Router

    init {
        WeatherApp.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showForecast()

    }

    fun onRefresh() {
        showForecast()
    }

    fun setScrollPosition(scrollPosition: Parcelable) {
        viewState.setState(scrollPosition)
    }

    private fun showForecast() {
        weatherRepository.getForecast(cityId)
                .subscribe(object : DefaultObserver<List<Weather>>() {
                    override fun onNext(@NonNull weatherList: List<Weather>) {
                        viewState.setWeather(weatherList, cityId)
                    }

                    override fun onError(@NonNull e: Throwable) {
                        router.showSystemMessage("Error")
                    }

                    override fun onComplete() {

                    }
                })

    }

}
