package iartyst.weather.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

import javax.inject.Inject

import iartyst.weather.app.WeatherApp
import iartyst.weather.common.Const
import iartyst.weather.mvp.models.Weather
import iartyst.weather.mvp.views.ListAdapterView
import ru.terrakok.cicerone.Router

/**
 * Author: iartyst
 */

@InjectViewState
class ListAdapterPresenter : MvpPresenter<ListAdapterView>() {

    @Inject
    lateinit var router: Router

    init {
        WeatherApp.appComponent.inject(this)
    }

    fun onItemClicked(weather: Weather) {
        router.navigateTo(Const.NAV_ITEM, weather)
    }
}
