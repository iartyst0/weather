package iartyst.weather.mvp.views

import android.util.SparseArray

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

import iartyst.weather.mvp.models.Weather

/**
 * Author: iartyst
 */

@StateStrategyType(AddToEndSingleStrategy::class)
interface HomeView : MvpView {

    fun setCities(cities: SparseArray<Weather>)

    fun showRefreshButton()

    fun hideRefreshButton()

    fun hideSpinner()

}
