package iartyst.weather.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Author iartyst
 */

@StateStrategyType(AddToEndSingleStrategy::class)
interface DetailView : MvpView {

    fun setMainInfo(info: String)

    fun setIcon(iconUrl: String)

    fun setDate(date: String)

    fun setPressure(pressure: Float?)

    fun setWindSpeed(speed: Float?)

}
