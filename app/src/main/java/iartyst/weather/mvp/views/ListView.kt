package iartyst.weather.mvp.views

import android.os.Parcelable

import com.arellomobile.mvp.MvpView

import iartyst.weather.mvp.models.Weather

/**
 * Author: iartyst
 */

interface ListView : MvpView {

    fun setWeather(weatherList: List<Weather>, cityId: Long)

    fun setState(state: Parcelable)
}
