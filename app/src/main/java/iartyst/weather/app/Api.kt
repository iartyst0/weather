package iartyst.weather.app

import io.reactivex.Observable
import iartyst.weather.mvp.models.gson.WeatherGroup
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Author: iartyst
 */

interface Api {

    @GET("group")
    fun getGroup(@Query("id") ids: String, @Query("units") units: String): Observable<WeatherGroup>

    @GET("forecast")
    fun getForecast(@Query("id") id: Long, @Query("units") units: String): Observable<WeatherGroup>

}
