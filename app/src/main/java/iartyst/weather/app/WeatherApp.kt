package iartyst.weather.app

import android.app.Application
import android.support.v7.app.AppCompatDelegate

import iartyst.weather.di.AppComponent
import iartyst.weather.di.DaggerAppComponent
import iartyst.weather.di.modules.AppModule

/**
 * Author: iartyst
 */

class WeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    companion object {
        lateinit var appComponent: AppComponent

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}
